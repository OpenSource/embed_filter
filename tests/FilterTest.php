<?php

namespace klambt\Tests;

use klambt\EmbedFilter\DefaultConfig;
use klambt\EmbedFilter\Filter;
use klambt\Tests\assets\TestPlaceholderFactory;
use PHPUnit\Framework\TestCase;

/**
 * Tests the behavior of the filter object.
 *
 * @coversDefaultClass \klambt\EmbedFilter\Filter
 * @group embed_filter_lib
 */
class FilterTest extends TestCase
{

    /**
     * The filter object to run tests on.
     *
     * @var \klambt\EmbedFilter\FilterInterface
     */
    protected $filter;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->filter = new Filter();
    }

    /**
     * Tests the default state of the filter object.
     *
     * @covers ::__construct()
     * @covers ::getConfig()
     */
    public function testConstructor()
    {
        // Test that a default base configuration object is present.
        $this->assertEquals(new DefaultConfig(), $this->filter->getConfig());

        // Test that you can pass a configuration object to the constructor.
        $custom_config = $this->getMockBuilder('\klambt\EmbedFilter\ConfigInterface')->getMock();
        $filter = new Filter($custom_config);
        $this->assertSame($custom_config, $filter->getConfig());
    }
    
    /**
     * Tests the setter for configuration objects.
     *
     * @covers ::setConfig()
     */
    public function testSetConfig()
    {
        $custom_config = $this->getMockBuilder('\klambt\EmbedFilter\ConfigInterface')->getMock();
        $this->filter->setConfig($custom_config);
        $this->assertEquals($custom_config, $this->filter->getConfig());
    }

    /**
     * Tests the identification of external URLs.
     *
     * @covers ::isExternalUrl()
     * @covers ::isRelativeUrl()
     */
    public function testIsExternalUrl()
    {
        // @todo Improve below assertions once we ditch regex.
        $this->filter->getConfig()
            ->addInternalDomain('(.*)\.test\.de')
            ->addInternalDomain('\.test-2\.com')
            ->addInternalDomain('www\.test-3\.com');

        // Test relative URLs.
        $this->assertFalse($this->filter->isExternalUrl('/'));
        $this->assertFalse($this->filter->isExternalUrl('/whatever/page.html'));

        // Test internal domains.
        $this->assertFalse($this->filter->isExternalUrl('http://www.test-3.com/whatever/page.html'));
        $this->assertFalse($this->filter->isExternalUrl('https://www.test-3.com'));
        $this->assertFalse($this->filter->isExternalUrl('//www.test-3.com:8080/whatever/page.html'));

        // Url not valid as it is missing the protocol.
        $this->assertFalse($this->filter->isExternalUrl('www.test-3.com:8080/whatever/page.html'));

        // Test external URL with protocol.
        $this->assertTrue($this->filter->isExternalUrl('http://origin.test-3.com/whatever/page.html'));
        $this->assertTrue($this->filter->isExternalUrl('https://origin.test-3.com'));
        $this->assertTrue($this->filter->isExternalUrl('//origin.test-3.com:8080/whatever/page.html'));

        // Test any subdomain of an internal URL.
        $this->assertFalse($this->filter->isExternalUrl('http://www.test.de/whatever/page.html'));
        $this->assertFalse($this->filter->isExternalUrl('https://www.test.de'));
        $this->assertFalse($this->filter->isExternalUrl('http://origin.test.de/whatever/page.html'));
        $this->assertFalse($this->filter->isExternalUrl('https://cdn.test.de'));
        $this->assertFalse($this->filter->isExternalUrl('http://www.test.de/whatever/page.html'));
        $this->assertFalse($this->filter->isExternalUrl('https://www.test.de'));
        $this->assertFalse($this->filter->isExternalUrl('//images.test.de:8080/whatever/page.html'));

        // Test external URL.
        $this->assertTrue($this->filter->isExternalUrl('http://test.de/whatever/page.html'));
        $this->assertTrue($this->filter->isExternalUrl('https://test.de'));
        $this->assertTrue($this->filter->isExternalUrl('//test.de:8080/whatever/page.html'));
    }

    /**
     * Tests filtering of known and unknown providers.
     *
     * @dataProvider externalHtmlSnippetDataProvider
     * @dataProvider externalHtmlPagesDataProvider
     *
     * @covers ::setHtml()
     * @covers ::filterHtml()
     */
    public function testFilterHtml($html_snippet, $provider)
    {
        $this->filter->setHtml($html_snippet);

        if (!$this->filter->getConfig()->getProviderRegistry()->hasProvider($provider)) {
            $provider = 'unknown';
        }
        $this->filter->getConfig()->setPlaceholderFactory(new TestPlaceholderFactory());
        $filtered_html = $this->filter->filterHtml();

        $this->assertContains(
            '---PLACEHOLDER--' . $provider . '---',
            $filtered_html,
            'Filtered html does not contain placeholder ' . $filtered_html
        );
    }

    /**
     * Tests filtering on domains marked as internal.
     *
     * @dataProvider externalWhitelistedHtmlSnippetDataProvider
     *
     * @covers ::setHtml()
     * @covers ::filterHtml()
     */
    public function testFilterHtmlOnInternalDomains($html_snippet, $provider)
    {
        $this->filter->setHtml($html_snippet);
        $this->filter->getConfig()->getProviderRegistry()
            ->removeProvider('instagram')
            ->removeProvider('facebook')
            ->removeProvider('twitter')
            ->removeProvider('youtube');
        $this->filter->getConfig()
            ->addInternalDomain('(.*)\.etonline\.com')
            ->addInternalDomain('(.*)\.instagram\.com')
            ->addInternalDomain('(.*)\.facebook\.com')
            ->addInternalDomain('(.*)\.brightcove\.com')
            ->addInternalDomain('(.*)\.twitter\.com')
            ->addInternalDomain('(.*)\.youtube\.com');

        $this->filter->getConfig()->setPlaceholderFactory(new TestPlaceholderFactory());
        $filtered_html = $this->filter->filterHtml();

        $this->assertNotContains(
            '---PLACEHOLDER--' . $provider . '---',
            $filtered_html,
            'Filtered html contains placeholder ' . $filtered_html
        );
    }

    public function externalHtmlSnippetDataProvider()
    {
        return $this->loadHtmlSnippets('external-snippets');
    }

    public function externalHtmlPagesDataProvider()
    {
        return $this->loadHtmlSnippets('external-pages');
    }

    public function externalWhitelistedHtmlSnippetDataProvider()
    {
        return $this->loadHtmlSnippets('external-snippets-whitelisted');
    }

    /**
     * Retrieves all snippets from the provided assets subdirectory.
     *
     * @param string $folder
     *   The name of the subdirectory to load all HTML files from.
     *
     * @return string[]
     *   A list of HTML snippets.
     *
     * @throws \Exception
     *   Throws a regular exception when a folder was not found.
     */
    private function loadHtmlSnippets($folder)
    {
        $path = dirname(__FILE__);
        $path .= '/assets/' . $folder;
        if (!is_dir($path)) {
            throw new \Exception('Directory not found: ' . $path);
        }
        $result = [];
        $dir = new \DirectoryIterator($path);
        foreach ($dir as $fileinfo) {
            if (!$fileinfo->isDot() && $fileinfo->getExtension() == 'html') {
                list($provider) = explode('-', $fileinfo->getFilename());
                $result[$fileinfo->getFilename()] = [
                    file_get_contents($path . '/' . $fileinfo->getFilename()),
                    $provider,
                ];
            }
        }
        return $result;
    }

}
