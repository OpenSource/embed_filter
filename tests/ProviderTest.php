<?php

namespace klambt\Tests;

use klambt\EmbedFilter\Provider;
use klambt\EmbedFilter\SourceElement;
use PHPUnit\Framework\TestCase;

/**
 * Tests the general behavior of a third party provider object.
 *
 * @coversDefaultClass \klambt\EmbedFilter\Provider
 * @group embed_filter_lib
 */
class ProviderTest extends TestCase
{

    /**
     * The provider object to run tests on.
     *
     * @var \klambt\EmbedFilter\Provider
     */
    protected $provider;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->provider = new Provider('foo');
    }

    /**
     * Tests that the ID was set in the constructor.
     *
     * @covers ::__construct()
     * @covers ::getId()
     */
    public function testConstructor()
    {
        $this->assertEquals('foo', $this->provider->getId());
    }

    /**
     * Tests setting the provider's regular expressions.
     *
     * @covers ::setRegularExpressions()
     * @covers ::getRegularExpressions()
     */
    public function testSetRegularExpressions()
    {
        $expressions = ['example.com\/', '(.*)\.example.com\/'];
        $this->provider->setRegularExpressions($expressions);
        $this->assertSame($expressions, $this->provider->getRegularExpressions());
    }

    /**
     * Tests that additional regular expressions can be added to the list.
     *
     * @covers ::addRegularExpression()
     */
    public function testAddRegularExpression()
    {
        $expressions = $this->provider->getRegularExpressions();
        $this->provider->addRegularExpression('(.*)\.example.com\/');
        $expressions[] = '(.*)\.example.com\/';
        $this->assertEquals($expressions, $this->provider->getRegularExpressions());
    }

    /**
     * Tests that URL matching works properly.
     *
     * @covers ::matchesUrl()
     */
    public function testMatchesUrl()
    {
        $expressions = ['example.com', '(.*)\.example.com'];
        $this->provider->setRegularExpressions($expressions);
        $this->assertTrue($this->provider->matchesUrl('http://foo.example.com'));
        $this->assertTrue($this->provider->matchesUrl('http://foo.example.com/bar'));
        $this->assertFalse($this->provider->matchesUrl('http://foo.example2.com'));
        $this->assertFalse($this->provider->matchesUrl('http://foo.example2.com/bar'));
    }

    /**
     * Tests the setter for source elements.
     *
     * @covers ::setSourceElements()
     * @covers ::getSourceElements()
     */
    public function testSetSourceElements()
    {
        $elements = [
            new SourceElement('//a[@href]'),
            new SourceElement('//img[@src]')
        ];
        $this->provider->setSourceElements($elements);
        $this->assertSame($elements, $this->provider->getSourceElements());
    }

    /**
     * Tests that additional source elements can be added to the list.
     *
     * @covers ::addSourceElement()
     */
    public function testAddSourceElement()
    {
        $elements = $this->provider->getSourceElements();
        $this->provider->addSourceElement(new SourceElement('//a[@href]'));
        $elements[] = new SourceElement('//a[@href]');
        $this->assertEquals($elements, $this->provider->getSourceElements());
    }

}
