<?php

namespace klambt\Tests;

use klambt\EmbedFilter\Provider;
use klambt\EmbedFilter\ProviderRegistry;
use PHPUnit\Framework\TestCase;

/**
 * Tests the general behavior of the provider registry.
 *
 * @coversDefaultClass \klambt\EmbedFilter\ProviderRegistry
 * @group embed_filter_lib
 */
class ProviderRegistryTest extends TestCase
{
    /**
     * The provider registry object to run tests on.
     *
     * @var \klambt\EmbedFilter\ProviderRegistry
     */
    protected $registry;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->registry = new ProviderRegistry();
    }

    /**
     * Tests adding a new provider to the registry.
     *
     * @covers ::addProvider()
     * @covers ::getProviders()
     */
    public function testAddNewProvider()
    {
        $this->registry->addProvider(new Provider('foo'));
        $this->assertEquals(['foo'], array_keys($this->registry->getProviders()));
    }

    /**
     * Tests adding an existing provider to the registry.
     *
     * @covers ::addProvider()
     * @expectedException \klambt\EmbedFilter\DuplicateProviderException
     * @expectedExceptionMessage Provider with ID 'foo' already exists in the registry.
     */
    public function testAddExistingProvider()
    {
        $this->registry->addProvider(new Provider('foo'));
        $this->registry->addProvider(new Provider('foo'));
    }

    /**
     * Tests the querying for known providers.
     *
     * @covers ::hasProvider()
     */
    public function testHasProvider()
    {
        $this->assertFalse($this->registry->hasProvider('foo'));
        $this->registry->addProvider(new Provider('foo'));
        $this->assertTrue($this->registry->hasProvider('foo'));
    }

    /**
     * Tests retrieving an existing provider from the registry.
     *
     * @covers ::getProvider()
     */
    public function testGetExistingProvider()
    {
        $this->registry->addProvider(new Provider('foo'));
        $this->assertInstanceOf('\klambt\EmbedFilter\Providerinterface', $this->registry->getProvider('foo'));
    }

    /**
     * Tests retrieving an unknown provider from the registry.
     *
     * @covers ::getProvider()
     * @expectedException \klambt\EmbedFilter\MissingProviderException
     * @expectedExceptionMessage Provider with ID 'foo' not found in the registry.
     */
    public function testGetUnknownProvider()
    {
        $this->registry->getProvider('foo');
    }

    /**
     * Tests removing an existing provider from the registry.
     *
     * @covers ::removeProvider()
     */
    public function testRemoveExistingProvider()
    {
        $this->registry->addProvider(new Provider('foo'));
        $this->registry->removeProvider('foo');
        $this->assertFalse($this->registry->hasProvider('foo'));
    }

    /**
     * Tests removing an unknown provider from the registry.
     *
     * @covers ::removeProvider()
     * @expectedException \klambt\EmbedFilter\MissingProviderException
     * @expectedExceptionMessage Provider with ID 'foo' not found in the registry.
     */
    public function testRemoveUnknownProvider()
    {
        $this->registry->removeProvider('foo');
    }

}
