<?php

namespace klambt\Tests;

use PHPUnit\Framework\TestCase;
use klambt\EmbedFilter\BasePlaceholderFactory;

/**
 * Tests the general behavior of the base placeholder factory object.
 *
 * @coversDefaultClass \klambt\EmbedFilter\BasePlaceholderFactory
 * @group embed_filter_lib
 */
class BasePlaceholderFactoryTest extends TestCase
{
    /**
     * The base placeholder factory object to run tests on.
     *
     * @var \klambt\EmbedFilter\PlaceholderFactoryInterface
     */
    protected $factory;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->factory = new BasePlaceholderFactory();
    }

    /**
     * Tests the creation of placeholders.
     *
     * @covers ::createPlaceholder()
     */
    public function testCreatePlaceholder()
    {
        $html = '<script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>';
        $this->assertEquals(
            '<ins data-provider="jquery" data-html="{&quot;html&quot;:&quot;&lt;script src=\&quot;https:\/\/code.jquery.com\/jquery-3.3.1.min.js\&quot;&gt;&lt;\/script&gt;&quot;}"></ins>',
            $this->factory->createPlaceholder($html, 'jquery')
        );
    }
}
