<?php

namespace klambt\Tests;

use klambt\EmbedFilter\BasePlaceholderFactory;
use klambt\EmbedFilter\DefaultConfig;
use klambt\EmbedFilter\ProviderRegistry;
use klambt\EmbedFilter\SourceElement;
use PHPUnit\Framework\TestCase;
use klambt\EmbedFilter\BaseConfig;

/**
 * Tests the general behavior of the base configuration object.
 *
 * @coversDefaultClass \klambt\EmbedFilter\BaseConfig
 * @group embed_filter_lib
 */
final class BaseConfigTest extends TestCase
{

    /**
     * The base config object to run tests on.
     *
     * @var \klambt\EmbedFilter\ConfigInterface
     */
    protected $config;

    /**
     * {@inheritdoc}
     */
    public function setUp()
    {
        $this->config = new DefaultConfig();
    }

    /**
     * Tests that you can pass information to the constructor.
     *
     * @covers ::__construct()
     * @covers ::getInternalDomains()
     * @covers ::getSourceAttributeXPaths()
     * @covers ::getPlaceholderFactory()
     * @covers ::getProviderRegistry()
     */
    public function testConstructor()
    {
        $internal_domains = $xpaths = [];
        $placeholder_factory = $this->getMockBuilder('\klambt\EmbedFilter\PlaceholderFactoryInterface')->getMock();
        $provider_registry = $this->getMockBuilder('\klambt\EmbedFilter\ProviderRegistryInterface')->getMock();

        $config = new BaseConfig($internal_domains, $xpaths, $placeholder_factory, $provider_registry);
        $this->assertSame($internal_domains, $config->getInternalDomains());
        $this->assertSame($xpaths, $config->getSourceAttributeXPaths());
        $this->assertSame($placeholder_factory, $config->getPlaceholderFactory());
        $this->assertSame($provider_registry, $config->getProviderRegistry());
    }

    /**
     * Tests the setter for internal domains.
     *
     * @covers ::setInternalDomains()
     */
    public function testSetInternalDomains()
    {
        $domains = ['www\.test\.de', '(.*)\.test-b\.de'];
        $this->config->setInternalDomains($domains);
        $this->assertSame($domains, $this->config->getInternalDomains());
    }

    /**
     * Tests that additional internal domains can be added to the list.
     *
     * @covers ::addInternalDomain()
     */
    public function testAddInternalDomain()
    {
        $domains = $this->config->getInternalDomains();
        $this->config->addInternalDomain('www\.test\.de');
        $domains[] = 'www\.test\.de';
        $this->assertEquals($domains, $this->config->getInternalDomains());
    }

    /**
     * Tests the setter for internal domains.
     *
     * @covers ::setSourceAttributeXPaths()
     */
    public function testSetSourceAttributeXPaths()
    {
        $xpaths = ['//img/@src', '//iframe/@src'];
        $this->config->setSourceAttributeXPaths($xpaths);
        $this->assertSame($xpaths, $this->config->getSourceAttributeXPaths());
    }

    /**
     * Tests that additional internal domains can be added to the list.
     *
     * @covers ::addSourceAttributeXPath()
     */
    public function testAddSourceAttributeXPath()
    {
        $xpaths = $this->config->getSourceAttributeXPaths();
        $this->config->addSourceAttributeXPath('//div/@data-url');
        $xpaths[] = '//div/@data-url';
        $this->assertEquals($xpaths, $this->config->getSourceAttributeXPaths());
    }

    /**
     * Tests the setter for the placeholder factory.
     *
     * @covers ::setPlaceholderFactory()
     */
    public function testSetPlaceholderFactory()
    {
        $placeholder_factory = new BasePlaceholderFactory();
        $this->config->setPlaceholderFactory($placeholder_factory);
        $this->assertSame($placeholder_factory, $this->config->getPlaceholderFactory());
    }

    /**
     * Tests the setter for the provider registry.
     *
     * @covers ::setProviderRegistry()
     */
    public function testSetProviderRegistry()
    {
        $provider_registry = new ProviderRegistry();
        $this->config->setProviderRegistry($provider_registry);
        $this->assertSame($provider_registry, $this->config->getProviderRegistry());
    }

}
