<?php

namespace klambt\Tests\assets;

use klambt\EmbedFilter\BasePlaceholderFactory;

/**
 * Simple test class that creates easy to read placeholders.
 */
class TestPlaceholderFactory extends BasePlaceholderFactory
{

    /**
     * {@inheritdoc}
     */
    public function createPlaceholder($html_to_hide, $provider = 'unknown')
    {
        return '---PLACEHOLDER--' . $provider .'---';
    }

}
