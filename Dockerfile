FROM php:7-stretch
ENV TZ=Europe/Berlin

MAINTAINER Tim Weyand

ADD ./ /var/www

RUN apt-get update \
 && apt-get -y upgrade \
 && apt-get install -y wget git zip \
 && wget -qO- https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
 && chown www-data:www-data /var/www

USER www-data

RUN cd /var/www/ \
 && composer install

WORKDIR /var/www