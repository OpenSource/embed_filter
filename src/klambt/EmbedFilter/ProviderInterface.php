<?php

namespace klambt\EmbedFilter;

/**
 * Defines the interface for providers.
 */
interface ProviderInterface
{

    /**
     * Retrieves the provider ID.
     *
     * @return string
     */
    public function getId();

    /**
     * Sets the regular expressions that will be used to match the provider.
     *
     * @param string[] $expressions
     *
     * @return self
     */
    public function setRegularExpressions($expressions);

    /**
     * Adds a single regular expression to the list.
     *
     * @param string $expression
     *
     * @return self
     */
    public function addRegularExpression($expression);

    /**
     * Retrieves the regular expressions.
     *
     * @return string[]
     */
    public function getRegularExpressions();

    /**
     * Checks whether a given URL matches the provider.
     *
     * @param string $url
     *
     * @return bool
     */
    public function matchesUrl($url);

    /**
     * Sets the source elements to check for external URLs.
     *
     * @param SourceElementInterface[] $elements
     *   The source elements to check.
     *
     * @return self
     */
    public function setSourceElements(array $elements);

    /**
     * Adds a single source element to the list.
     *
     * @param SourceElementInterface $element
     *
     * @return self
     */
    public function addSourceElement(SourceElementInterface $element);

    /**
     * Retrieves the source elements to check.
     *
     * @return SourceElementInterface[]
     */
    public function getSourceElements();

}
