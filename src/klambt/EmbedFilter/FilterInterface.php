<?php

namespace klambt\EmbedFilter;

/**
 * Defines the interface for external sources filters.
 */
interface FilterInterface
{

    const ABSOLUTE_URL_PATTERN = '^(http:\/|https:\/|\/)\/';
    const UNKNOWN_PROVIDER = 'unknown';

    /**
     * Sets the filter configuration.
     *
     * @param ConfigInterface $config
     *   The configuration object representing the filter configuration.
     */
    public function setConfig(ConfigInterface $config);

    /**
     * Retrieves the filter configuration.
     *
     * @return ConfigInterface
     */
    public function getConfig();

    /**
     * Sets the HTML to filter.
     *
     * @param string $html
     *   The HTML to filter.
     *
     * @return self
     */
    public function setHtml($html);

    /**
     * Check if given url is external.
     *
     * @param string $url
     *   The URL to check.
     *
     * @return boolean
     *   Whether the URL is considered external.
     */
    public function isExternalUrl($url);

    /**
     * Filters the loaded HTML, replacing external sources with placeholders.

     * @return string
     *   The filtered HTML.
     */
    public function filterHtml();

}
