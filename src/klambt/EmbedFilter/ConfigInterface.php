<?php

namespace klambt\EmbedFilter;

/**
 * Defines the interface for configuration objects.
 */
interface ConfigInterface
{

    /**
     * Sets the domains to recognize as internal.
     *
     * @param string[] $domains
     *   The internal domains, currently represented by regular expressions.
     *
     * @return self
     */
    public function setInternalDomains(array $domains);

    /**
     * Adds a single internal domain to the list.
     *
     * @param string $domain
     *
     * @return self
     */
    public function addInternalDomain($domain);

    /**
     * Retrieves the internal domains.
     *
     * @return string[]
     */
    public function getInternalDomains();

    /**
     * Sets the XPaths to find source attributes with.
     *
     * This will be used to find external sources after all of the providers
     * have been processed already. Any caught attribute's parent node will
     * then be filtered out as unknown.
     *
     * @param string[] $xpaths
     *   The XPath expressions to find external sources with.
     *
     * @return self
     */
    public function setSourceAttributeXPaths(array $xpaths);

    /**
     * Adds a single Xpath expression for finding external sources.
     *
     * @param string $xpath
     *
     * @return self
     */
    public function addSourceAttributeXPath($xpath);

    /**
     * Retrieves the source attribute XPath expressions to check.
     *
     * @return string[]
     */
    public function getSourceAttributeXPaths();

    /**
     * Sets the placeholder factory to generate placeholders with.
     *
     * @param PlaceholderFactoryInterface $placeholder_factory
     *
     * @return self
     */
    public function setPlaceholderFactory(PlaceholderFactoryInterface $placeholder_factory);

    /**
     * Retrieves the placeholder factory.
     *
     * @return PlaceholderFactoryInterface
     */
    public function getPlaceholderFactory();

    /**
     * Sets the third party provider registry.
     *
     * @param ProviderRegistryInterface $registry
     *
     * @return self
     */
    public function setProviderRegistry(ProviderRegistryInterface $registry);

    /**
     * Retrieves the third party provider registry.
     *
     * @return ProviderRegistryInterface
     */
    public function getProviderRegistry();

}
