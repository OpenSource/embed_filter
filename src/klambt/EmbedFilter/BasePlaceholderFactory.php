<?php

namespace klambt\EmbedFilter;

class BasePlaceholderFactory implements PlaceholderFactoryInterface
{

    public function createPlaceholder($html_to_hide, $provider = 'unknown')
    {
        $escaped_html = htmlspecialchars(json_encode(['html' => $html_to_hide]), ENT_QUOTES, 'UTF-8');
        return "<ins data-provider=\"$provider\" data-html=\"$escaped_html\"></ins>";
    }
}
