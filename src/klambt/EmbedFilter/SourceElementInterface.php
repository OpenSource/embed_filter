<?php

namespace klambt\EmbedFilter;

/**
 * Defines the interface for source elements.
 *
 * This allows the creation of value objects that tell you how to find a
 * specific element in a DOM structure that might represent an external source.
 */
interface SourceElementInterface
{

    /**
     * Retrieves the element XPath query.
     *
     * @return string
     */
    public function getElementXPath();

    /**
     * Generates the HTML to replace for a matched element.
     *
     * If the matched element is comprised of several sibling elements, it is
     * advised to remove the siblings that are not represented by $element from
     * the document as you add them to the document fragment. This will ensure
     * that no partial HTML is left behind after placeholdering.
     *
     * If the whole external source can be replaced by selecting a single root
     * element, then please for the love of all that is holy provide an element
     * XPath that selects said root element and use the provided SourceElement
     * class.
     *
     * @param \DOMNode $element
     *   The matched element.
     * @param \DOMDocument $document
     *   The owner document.
     *
     * @return string
     *   The HTML to replace.
     */
    public function getHtmlToReplace(\DOMNode $element, \DOMDocument $document);

}
