<?php

namespace klambt\EmbedFilter;

/**
 * External sources filter.
 *
 * Tracks down external sources in HTML and replaces it with placeholders.
 */
class Filter implements FilterInterface
{

    /**
     * The configuration for the filter.
     *
     * @var ConfigInterface
     */
    protected $config;

    /**
     * The DOM document wrapper for the HTML to filter.
     *
     * @var \DOMDocument
     */
    protected $document;

    /**
     * The XPath object for the DOM document.
     *
     * @var \DOMXPath
     */
    protected $xpath;

    /**
     * Filter constructor.
     *
     * @param ConfigInterface|null $config
     *   (optional) The configuration to use when filtering. Leave blank to use
     *   a clean BaseConfig instance as the default.
     */
    public function __construct(ConfigInterface $config = null)
    {
        $this->setConfig($config ? $config : new DefaultConfig());
    }

    /**
     * {@inheritdoc}
     */
    public function setConfig(ConfigInterface $config)
    {
        $this->config = $config;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * {@inheritdoc}
     */
    public function setHtml($html)
    {
        $this->document = new \DOMDocument();
        libxml_use_internal_errors(true);
        $this->document->loadHTML($html);
        libxml_use_internal_errors(false);
        $this->xpath = new \DOMXPath($this->document);
        return $this;
    }

    /**
     * Checks whether a URL is a relative path.
     *
     * @param string $url
     *   The URL to check.
     *
     * @return bool
     *   Whether the URL is relative.
     *
     * @todo Use parse_url().
     */
    private function isRelativeUrl($url)
    {
        $pattern = '/' . FilterInterface::ABSOLUTE_URL_PATTERN  . '/';
        if (!preg_match($pattern, $url)) {
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function isExternalUrl($url)
    {
        if ($this->isRelativeUrl($url)) {
            return false;
        }

        foreach ($this->getConfig()->getInternalDomains() as $domain) {
            // Check with protocol or protocol-relative urls.
            $pattern = '/' . FilterInterface::ABSOLUTE_URL_PATTERN . $domain . '/';
            if (preg_match($pattern, $url)) {
                return false;
            }
        }
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public function filterHtml()
    {
        $placeholderFactory = $this->getConfig()->getPlaceholderFactory();

        // First allow all providers to create provider-specific placeholders.
        foreach ($this->getConfig()->getProviderRegistry()->getProviders() as $provider) {
            foreach ($provider->getSourceElements() as $element) {
                $result = $this->xpath->query($element->getElementXPath());
                foreach ($result as $old_element) {
                    $replacement = $element->getHtmlToReplace($old_element, $this->document);
                    $new_element = $this->document->createDocumentFragment();
                    $new_element->appendXML($placeholderFactory->createPlaceholder($replacement, $provider->getId()));
                    $old_element->parentNode->replaceChild($new_element, $old_element);
                }
            }
        }

        // Now catch all possibly dangerous sources and if they are external,
        // placeholder them as unknown.
        $query = implode(' | ', $this->getConfig()->getSourceAttributeXPaths());
        $result = $this->xpath->query($query);
        foreach ($result as $attribute) {
            $url = $attribute->nodeValue;
            if ($this->isExternalUrl($url)) {
                $old_element = $attribute->ownerElement;
                $new_element = $this->document->createDocumentFragment();
                $new_element->appendXML($placeholderFactory->createPlaceholder($this->document->saveHTML($old_element), FilterInterface::UNKNOWN_PROVIDER));
                $old_element->parentNode->replaceChild($new_element, $old_element);
            }
        }

        return $this->document->saveHTML();
    }

}
