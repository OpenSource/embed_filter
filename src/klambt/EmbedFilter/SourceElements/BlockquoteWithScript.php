<?php

namespace klambt\EmbedFilter\SourceElements;

use klambt\EmbedFilter\SourceElement;

/**
 * Source element handler for blockquotes followed by a script.
 *
 * Suggested XPath to pass to the constructor is:
 * @code
 * //blockquote/following-sibling::script[YOUR CONDITION HERE]
 * @endcode
 *
 * If you have a more complex or performant XPath, feel free to use it instead.
 * Just keep in mind that this class expects the selected element to be a
 * script tag and then looks for a blockquote as the previous sibling.
 */
class BlockquoteWithScript extends SourceElement
{

    /**
     * {@inheritdoc}
     */
    public function getHtmlToReplace(\DOMNode $element, \DOMDocument $document)
    {
        // Find the previous sibling blockquote, disregarding text nodes.
        $blockquote = $element->previousSibling;
        while ($blockquote !== null && $blockquote->nodeName == '#text' && !empty($blockquote->previousSibling)) {
            $blockquote = $blockquote->previousSibling;
        }

        if (!empty($blockquote) && strtolower($blockquote->nodeName) == 'blockquote') {
            // By appending the blockquote to a document fragment, we remove it
            // from its original place in the DOM.
            $fragment = $document->createDocumentFragment();
            $fragment->appendChild($blockquote);
            return $document->saveHTML($fragment) . $document->saveHTML($element);
        }

        return parent::getHtmlToReplace($element, $document);
    }

}
