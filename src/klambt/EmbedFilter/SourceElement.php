<?php

namespace klambt\EmbedFilter;

/**
 * Wrapper for source elements.
 */
class SourceElement implements SourceElementInterface
{

    /**
     * The element XPath query.
     *
     * @var string
     */
    private $elementXPath;

    /**
     * SourceElement constructor.
     *
     * @param string $elementXPath
     *   The element XPath query.
     */
    public function __construct($elementXPath)
    {
        $this->elementXPath = $elementXPath;
    }

    /**
     * {@inheritdoc}
     */
    public function getElementXPath()
    {
        return $this->elementXPath;
    }

    /**
     * {@inheritdoc}
     */
    public function getHtmlToReplace(\DOMNode $element, \DOMDocument $document)
    {
        return $document->saveHTML($element);
    }

}
