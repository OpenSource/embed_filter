<?php

namespace klambt\EmbedFilter;

/**
 * Registry for third party providers.
 *
 * Ensures uniqueness across all registered providers.
 */
class ProviderRegistry implements ProviderRegistryInterface
{

    /**
     * A list of registered providers, keyed by provider ID.
     *
     * @var ProviderInterface[]
     */
    protected $providers = [];

    /**
     * {@inheritdoc}
     */
    public function hasProvider($id)
    {
        return isset($this->providers[$id]);
    }

    /**
     * {@inheritdoc}
     */
    public function addProvider(ProviderInterface $provider)
    {
        $id = $provider->getId();
        if ($this->hasProvider($id)) {
            throw new DuplicateProviderException("Provider with ID '$id' already exists in the registry.");
        }
        $this->providers[$id] = $provider;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getProvider($id)
    {
        if (!$this->hasProvider($id)) {
            throw new MissingProviderException("Provider with ID '$id' not found in the registry.");
        }
        return $this->providers[$id];
    }

    /**
     * {@inheritdoc}
     */
    public function getProviders()
    {
        return $this->providers;
    }

    /**
     * {@inheritdoc}
     */
    public function removeProvider($id)
    {
        if (!$this->hasProvider($id)) {
            throw new MissingProviderException("Provider with ID '$id' not found in the registry.");
        }
        unset($this->providers[$id]);
        return $this;
    }
}
