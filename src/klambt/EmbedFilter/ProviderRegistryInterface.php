<?php

namespace klambt\EmbedFilter;

/**
 * Defines the interface for a provider registry.
 */
interface ProviderRegistryInterface
{

    /**
     * Checks if a provider exists in the registry.
     *
     * @param string $id
     *   The ID of the provider to check for.
     *
     * @return boolean
     *   Whether the requested provider exists in the registry.
     */
    public function hasProvider($id);

    /**
     * Adds a provider to the registry.
     *
     * @param ProviderInterface $provider
     *   The unique ID for the provider.
     *
     * @return self
     *
     * @throws DuplicateProviderException
     */
    public function addProvider(ProviderInterface $provider);

    /**
     * Retrieves a provider from the registry.
     *
     * @param string $id
     *   The ID of the provider to retrieve.
     *
     * @return ProviderInterface
     *   The requested provider.
     *
     * @throws MissingProviderException
     */
    public function getProvider($id);

    /**
     * Retrieves a list of all registered providers.
     *
     * @return ProviderInterface[]
     *   The registered providers, keyed by their provider ID.
     */
    public function getProviders();

    /**
     * Removes a provider from the registry.
     *
     * @param string $id
     *   The ID of the provider to remove.
     *
     * @return self
     *
     * @throws MissingProviderException
     */
    public function removeProvider($id);

}
