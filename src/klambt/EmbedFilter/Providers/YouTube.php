<?php

namespace klambt\EmbedFilter\Providers;

use klambt\EmbedFilter\Provider;
use klambt\EmbedFilter\SourceElement;

/**
 * Provider implementation for YouTube.
 */
class YouTube extends Provider
{

    /**
     * YouTube constructor.
     */
    public function __construct()
    {
        parent::__construct('youtube');

        $options = [
            'http://www.youtube.com/',
            'http://youtube.com/',
            'https://www.youtube.com/',
            'https://youtube.com/',
            '//www.youtube.com/',
            '//youtube.com/',
            'http://www.youtu.be/',
            'http://youtu.be/',
            'https://www.youtu.be/',
            'https://youtu.be/',
            '//www.youtu.be/',
            '//youtu.be/',
        ];
        
        $this->setSourceElements([
            new SourceElement('//iframe[' . $this->startsWithCondition('@src', $options) . ']'),
            new SourceElement('//embed[' . $this->startsWithCondition('@src', $options) . ']/parent::object'),
        ]);
    }

}
