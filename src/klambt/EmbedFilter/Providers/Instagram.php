<?php

namespace klambt\EmbedFilter\Providers;

use klambt\EmbedFilter\Provider;
use klambt\EmbedFilter\SourceElement;
use klambt\EmbedFilter\SourceElements\BlockquoteWithScript;

/**
 * Provider implementation for Instagram.
 */
class Instagram extends Provider
{

    /**
     * Instagram constructor.
     */
    public function __construct()
    {
        parent::__construct('instagram');

        $options = [
            'http://www.instagram.com/',
            'http://instagram.com/',
            'https://www.instagram.com/',
            'https://instagram.com/',
            '//www.instagram.com/',
            '//instagram.com/',
        ];

        $this->setSourceElements([
            // @todo Might want to use contains() to avoid issues with the language.
            new BlockquoteWithScript('//blockquote/following-sibling::script[' . $this->endsWithCondition('@src', ['//platform.instagram.com/en_US/embeds.js']) . ']'),
            new SourceElement('//blockquote[@class="instagram-media" and ' . $this->startsWithCondition('@data-instgrm-permalink', $options) . ']'),
        ]);
    }

}
