<?php

namespace klambt\EmbedFilter\Providers;

use klambt\EmbedFilter\Provider;
use klambt\EmbedFilter\SourceElements\BlockquoteWithScript;

/**
 * Provider implementation for Twitter.
 */
class Twitter extends Provider
{

    /**
     * Twitter constructor.
     */
    public function __construct()
    {
        parent::__construct('twitter');
        $this->setSourceElements([
            new BlockquoteWithScript('//blockquote/following-sibling::script[' . $this->endsWithCondition('@src', ['//platform.twitter.com/widgets.js']) . ']'),
        ]);
    }

}
