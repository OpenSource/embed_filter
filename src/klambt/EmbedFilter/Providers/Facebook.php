<?php

namespace klambt\EmbedFilter\Providers;

use klambt\EmbedFilter\Provider;
use klambt\EmbedFilter\SourceElement;

/**
 * Provider implementation for Facebook.
 */
class Facebook extends Provider
{

    /**
     * Facebook constructor.
     */
    public function __construct()
    {
        parent::__construct('facebook');

        $options = [
            'http://www.facebook.com/',
            'http://facebook.com/',
            'https://www.facebook.com/',
            'https://facebook.com/',
            '//www.facebook.com/',
            '//facebook.com/',
        ];

        $this->setSourceElements([
            new SourceElement('//iframe[' . $this->startsWithCondition('@src', $options) . ']'),
            new SourceElement('//div[@class="fb-post" and ' . $this->startsWithCondition('@data-href', $options) . ']'),
        ]);
    }

}
