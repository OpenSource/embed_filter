<?php

namespace klambt\EmbedFilter\Providers;

use klambt\EmbedFilter\Provider;
use klambt\EmbedFilter\SourceElement;

/**
 * Provider implementation for Vimeo.
 */
class Vimeo extends Provider
{

    const REGEX = '^((https?\:)?//)?player\.vimeo\.com\/video/.+$';

    /**
     * Vimeo constructor.
     */
    public function __construct()
    {
        parent::__construct('vimeo');

        $options = [
            'http://player.vimeo.com/video/',
            'https://player.vimeo.com/video/',
            '//player.vimeo.com/video/',
        ];

        $this->setSourceElements([
            new SourceElement('//iframe[' . $this->startsWithCondition('@src', $options) . ']'),
        ]);
    }

}
