<?php

namespace klambt\EmbedFilter;

/**
 * The base configuration class to extend when writing custom configuration.
 */
class BaseConfig implements ConfigInterface
{

    /**
     * The internal domains.
     *
     * @var string[]
     */
    protected $internalDomains = [];

    /**
     * The XPath queries for catching the unknown providers' source attributes.
     * @var string[]
     */
    protected $sourceAttributeXPaths = [];

    /**
     * The placeholder factory.
     *
     * @var PlaceholderFactoryInterface
     */
    protected $placeholderFactory = null;

    /**
     * The third party provider registry.
     *
     * @var ProviderRegistryInterface
     */
    protected $providerRegistry;

    /**
     * BaseConfig constructor.
     *
     * @param string[] $internal_domains
     *   The internal domains.
     * @param string[] $xpaths
     *   The XPath queries for finding the unknown providers.
     * @param PlaceholderFactoryInterface $placeholder_factory
     *   The placeholder factory.
     * @param ProviderRegistryInterface $provider_registry
     *   The third party provider registry.
     */
    public function __construct(array $internal_domains, array $xpaths, PlaceholderFactoryInterface $placeholder_factory, ProviderRegistryInterface $provider_registry)
    {
        $this->setInternalDomains($internal_domains);
        $this->setSourceAttributeXPaths($xpaths);
        $this->setPlaceholderFactory($placeholder_factory);
        $this->setProviderRegistry($provider_registry);
    }

    /**
     * {@inheritdoc}
     */
    public function setInternalDomains(array $domains)
    {
        $this->internalDomains = $domains;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addInternalDomain($domain)
    {
        $this->internalDomains[] = $domain;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getInternalDomains()
    {
        return $this->internalDomains;
    }

    /**
     * {@inheritdoc}
     */
    public function setSourceAttributeXPaths(array $xpaths)
    {
        $this->sourceAttributeXPaths = $xpaths;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function addSourceAttributeXPath($xpath)
    {
        $this->sourceAttributeXPaths[] = $xpath;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getSourceAttributeXPaths()
    {
        return $this->sourceAttributeXPaths;
    }

    /**
     * {@inheritdoc}
     */
    public function setPlaceholderFactory(PlaceholderFactoryInterface $placeholder_factory)
    {
        $this->placeholderFactory = $placeholder_factory;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getPlaceholderFactory()
    {
        return $this->placeholderFactory;
    }

    /**
     * {@inheritdoc}
     */
    public function setProviderRegistry(ProviderRegistryInterface $registry)
    {
        $this->providerRegistry = $registry;
        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function getProviderRegistry()
    {
        return $this->providerRegistry;
    }

}
