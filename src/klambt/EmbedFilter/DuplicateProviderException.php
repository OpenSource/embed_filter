<?php

namespace klambt\EmbedFilter;

use \Exception;

/**
 * Exception for reporting duplicate providers.
 */
class DuplicateProviderException extends Exception
{
}
