<?php

namespace klambt\EmbedFilter;

/**
 * Wrapper for third party providers.
 *
 * Do not instantiate directly, but use the ProviderRegistry class instead.
 */
class Provider implements ProviderInterface
{

    /**
     * The provider ID.
     *
     * @var string
     */
    private $id;

    /**
     * A list of regular expressions that can be used to match the provider.
     *
     * @var string[]
     */
    private $expressions = [];

    /**
     * The source elements to detect.
     *
     * @var SourceElementInterface[]
     */
    private $sourceElements = [];

    /**
     * Provider constructor.
     *
     * @param string $id
     *   The provider ID.
     */
    public function __construct($id)
    {
        $this->id = $id;
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * {@inheritdoc}
     */
    public function setRegularExpressions($expressions)
    {
        $this->expressions = $expressions;
    }

    /**
     * {@inheritdoc}
     */
    public function addRegularExpression($expression)
    {
        $this->expressions[] = $expression;
    }

    /**
     * {@inheritdoc}
     */
    public function getRegularExpressions()
    {
        return $this->expressions;
    }

    /**
     * {@inheritdoc}
     */
    public function matchesUrl($url)
    {
        // @todo Rework to use parse_url() instead.
        foreach ($this->getRegularExpressions() as $pattern) {
            if (preg_match('/'. FilterInterface::ABSOLUTE_URL_PATTERN . $pattern.'/', $url)) {
                return true;
            }
        }
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function setSourceElements(array $elements)
    {
        $this->sourceElements = $elements;
    }

    /**
     * {@inheritdoc}
     */
    public function addSourceElement(SourceElementInterface $element)
    {
        $this->sourceElements[] = $element;
    }

    /**
     * {@inheritdoc}
     */
    public function getSourceElements()
    {
        return $this->sourceElements;
    }

    /**
     * Helper function to generate a starts-with XPath condition.
     *
     * @param string $subject
     *   The thing to check.
     * @param string[] $options
     *   The strings the subject can start with.
     *
     * @return string
     *   The complete XPath condition.
     */
    protected function startsWithCondition($subject, array $options)
    {
        $result = [];
        foreach ($options as $option) {
            $result[] = "starts-with($subject, '$option')";
        }
        return '(' . implode(' or ', $result) . ')';
    }

    /**
     * Helper function to generate an ends-with XPath condition.
     *
     * Note that this is not supported by XPath 1.0, so we are using a
     * makeshift ends-with condition.
     *
     * @param string $subject
     *   The thing to check.
     * @param string[] $options
     *   The strings the subject can start with.
     *
     * @return string
     *   The complete XPath condition.
     */
    protected function endsWithCondition($subject, array $options)
    {
        $result = [];
        foreach ($options as $option) {
            $result[] = "substring($subject, string-length($subject) - string-length('$option') +1) = '$option'";
        }
        return '(' . implode(' or ', $result) . ')';
    }

}
