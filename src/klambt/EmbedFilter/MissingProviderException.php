<?php

namespace klambt\EmbedFilter;

use \Exception;

/**
 * Exception for reporting missing providers.
 */
class MissingProviderException extends Exception
{
}
