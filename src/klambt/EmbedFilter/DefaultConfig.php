<?php

namespace klambt\EmbedFilter;

use klambt\EmbedFilter\Providers\Facebook;
use klambt\EmbedFilter\Providers\Instagram;
use klambt\EmbedFilter\Providers\Twitter;
use klambt\EmbedFilter\Providers\Vimeo;
use klambt\EmbedFilter\Providers\YouTube;

/**
 * The default configuration class.
 */
class DefaultConfig extends BaseConfig
{

    /**
     * DefaultConfig constructor.
     */
    public function __construct()
    {
        $internal_domains = [];

        $xpaths = [
            '//div/@data-href',
            '//embed/@src',
            '//img/@src',
            '//iframe/@src',
            '//object/@data',
            '//object/@src',
            '//object/@value',
            '//script/@src',
        ];

        $provider_registry = new ProviderRegistry();
        $provider_registry->addProvider(new YouTube())
            ->addProvider(new Instagram())
            ->addProvider(new Twitter())
            ->addProvider(new Vimeo())
            ->addProvider(new Facebook());

        $placeholder_factory = new BasePlaceholderFactory();

        parent::__construct($internal_domains, $xpaths, $placeholder_factory, $provider_registry);
    }

}
