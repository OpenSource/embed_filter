<?php

namespace klambt\EmbedFilter;

interface PlaceholderFactoryInterface
{
    public function createPlaceholder($html_to_hide, $provider = 'unknown');
}
